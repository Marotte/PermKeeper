# PermKeeper

This script stores owner, group and permissions of all files in a given directory to restore them later.

The 'keep' and 'recover' actions can be used to store/recover the permissions in/from a '.pkeep' file inside the directory itself :

    $ pkeep keep -d /tmp

it will create a /tmp/.pkeep file storing the permissions of /tmp.

Then

    $ pkeep recover -d /tmp

will re-apply them.

Of course, it wont set permissions on newly created files.

The current directory is used if none is specified.

The 'list' action print out the permissions using tabulations, for a human readable output.

    $ pkeep list -d /tmp
    
You can use standard input/output with the 'store' and 'deploy' commands.
    
    $ pkeep store -d /var/www/html > file
    
    $ pkeep deploy < file
    
To check if the current permissions do not differ from what has been saved, it’s 'check' :

    $ pkeep check

will show you newly created files and files of which permissions have been modified in the current directory.

You may exclude some directories like this :

    $ pkeep keep -x .git -f 'keep1'
    
it would store the permissions of files of the current directory, into a file named 'keep1', except all files under the '.git' directory.
